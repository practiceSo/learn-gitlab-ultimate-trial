rovider "aws" {
    region = "ap-south-1"
    access_key = "AKIAWNKAPMDQKCWNRIWM"
    secret_key = "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"
    }
variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix {}

resource "aws_vpc" "sonu-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "${var.env_prefix}-vpc"
    }
}

module "sonu-subnet" {
    source = "modules/subnet"
    subnet_cidr_block = var.subnet_cidr_block
    avail_zone = var.avail_zone
    env_prefix = var.env_prefix
    vpc_id = aws_vpc.sonu-vpc.id
    default_route_table_id = aws_vpc.sonu-vpc.default_route_table_id
}

resource "aws_subnet" "sonu-subnet-1" {
    vpc_id = aws_vpc.sonu-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name: "${var.env_prefix}-subnet-1"
    }
} 


resource "aws_internet_gateway" "sonu-igw" {
    vpc_id = aws_vpc.sonu-vpc.id
     tags = {
        Name:  "${var.env_prefix}-igw"
    }
}

resource "aws_route_table" "sonu-route-table" {
    vpc_id = aws_vpc.sonu-vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.sonu-igw.id
    }
    tags = {
        Name:  "${var.env_prefix}-rtb"
    }
}
resource "aws_security_group" "sonu-sg" {
    name = "sonu-sg"
    vpc_id = aws_vpc.sonu-vpc.id

    # Ingress and Egress are Firewall Rules
    # Incoming traffic ( - ssh into EC2 and - access from browser)
    # who is allowed to access resource on port 22?
    # Ingress refers to the right to enter a property
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }

   
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    # Egress refers to the right to exit a property
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }

    tags = {
         Name: "${var.env_prefix}-default-sg"

    }
}
data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = []

    }
}
resource "aws_instance" "sonu-server" {
    ami = data.aws_ami.latest-amazon-linux-image.id
    instance_type = var.instance_type
}
